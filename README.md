# documentation-and-publishing

Just for checking how well (or bad) our documentation artifacts can be displayed in gitlab comments / md files etc.

![](https://gitlab.com/nunet/open-api/platform-data-model/-/raw/ca6711ceae2b41fdd3e4825fc06e203e73bc4866/device-management-service/onboarding/rendered/capacityForNunet.data.svg)